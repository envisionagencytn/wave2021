<?php

use Illuminate\Support\Facades\Route;

use App\Http\Controllers\TransportController;
use App\Http\Controllers\ServiceController;
use App\Http\Controllers\UserController;
use App\Http\Controllers\Controller;
use App\Http\Controllers\TypeProduitController;


/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::middleware(['auth:sanctum', 'verified'])->get('/dashboard', function () {
    return view('dashboard');
})->name('dashboard');


Route::group(['middleware' => 'auth'], function() {
    Route::group(['middleware' => 'role:superadmin', 'prefix' => 'superadmin', 'as' => 'superadmin.'], function() {
        Route::resource('superadmin', \App\Http\Controllers\Superadmin\SuperadminController::class);
    });
   Route::group(['middleware' => 'role:admin', 'prefix' => 'admin', 'as' => 'admin.'], function() {
       Route::resource('admin', \App\Http\Controllers\Admin\AdminController::class);
   });
    Route::group(['middleware' => 'role:client', 'prefix' => 'client', 'as' => 'client.'], function() {
        Route::resource('client', \App\Http\Controllers\Client\ClientController::class);
    });
});


Route::get('/parametre/service', [ServiceController::class, 'index']);
Route::get('/parametre/fourniture', [ServiceController::class, 'index']);
Route::get('/parametre/accessoire', [ServiceController::class, 'index']);

Route::get('/parametre/service/ajouter', [ServiceController::class, 'create']);
Route::get('/parametre/fourniture/ajouter', [ServiceController::class, 'create']);
Route::get('/parametre/accessoire/ajouter', [ServiceController::class, 'create']);



Route::get('/transport/moyen', [TransportController::class, 'index']);
Route::get('/transport/nature', [TransportController::class, 'index']);

Route::get('/transport/moyen/ajouter', [TransportController::class, 'create']);
Route::get('/transport/nature/ajouter', [TransportController::class, 'create']);


Route::get('/users', [UserController::class, 'index']);
