<?php

namespace Database\Seeders;

use App\Models\Role;
use Illuminate\Database\Seeder;

class RoleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Role::create(['name' => 'Super Administrator']);
        Role::create(['name' => 'Administrator']);
        Role::create(['name' => 'Financier']);
        Role::create(['name' => 'Magasinier']);
        Role::create(['name' => 'Technicien']);
        Role::create(['name' => 'Client']);
    }
}