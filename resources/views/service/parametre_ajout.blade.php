<x-app-layout>
   
            <!-- BEGIN: Content -->
            <div class="content">
                @include('layouts.header')

                
                <div class="intro-y flex items-center mt-8">
                    <h2 class="text-lg font-medium mr-auto">
                        Formulaire {{Request::segment(2)}}
                    </h2>
                </div>
                <div class="grid grid-cols-12 gap-6 mt-5">
                    <div class="intro-y col-span-12 lg:col-span-12">
                        <!-- BEGIN: Form Layout -->
                        <div class="intro-y box p-5">
                            <div>
                                <label>Nom de {{Request::segment(2)}}</label>
                                <input type="text" class="input w-full border mt-2" placeholder="Input text">
                            </div>
                            
                            <div class="text-right mt-5">
                                <button type="button" class="button w-24 border dark:border-dark-5 text-gray-700 dark:text-gray-300 mr-1">Annuler</button>
                                <button type="button" class="button w-24 bg-theme-1 text-white">Ajouter</button>
                            </div>
                        </div>
                        <!-- END: Form Layout -->
                    </div>
                </div>
                
               
            </div>
            <!-- END: Content -->
     
        
</x-app-layout>
