<?php

namespace App\Http\Controllers;

use App\Models\FamilleProduit;
use Illuminate\Http\Request;

class FamilleProduitController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\FamilleProduit  $familleProduit
     * @return \Illuminate\Http\Response
     */
    public function show(FamilleProduit $familleProduit)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\FamilleProduit  $familleProduit
     * @return \Illuminate\Http\Response
     */
    public function edit(FamilleProduit $familleProduit)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\FamilleProduit  $familleProduit
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, FamilleProduit $familleProduit)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\FamilleProduit  $familleProduit
     * @return \Illuminate\Http\Response
     */
    public function destroy(FamilleProduit $familleProduit)
    {
        //
    }
}
